data {
    int <lower=1> nObs;
    int <lower=1> N;

    real log_loss_paid_known[nObs];
    int<lower=1, upper=N> known_loss_ndxs[nObs];

    real log_premium[N];
    int<lower=1> year[N];
    int<lower=1> dev_lag[N];
}

transformed data {
    int nPred = N - nObs;
    int nYears = max(year);
    int nLags = max(dev_lag);
    int<lower=1, upper=N> pred_loss_ndxs[nPred];

    int i_ = 1;
    int iK = 1;
    int iP = 1;
    while (i_ <= N) {
        if (iK <= nObs && known_loss_ndxs[iK] == i_) {
            iK += 1;
        } else {
            pred_loss_ndxs[iP] = i_;
            iP += 1;
        }
        i_ += 1;
    }
}

parameters {
    real log_ELR;
    real alpha_free[nYears - 1];
    real beta_free[nLags - 1];
    real<lower=0, upper=100000> sigma_raw[nLags];
    real gamma;
    real log_loss_paid_pred[nPred];
}

transformed parameters {
    real alpha[nYears];
    real beta[nLags];
    real sigma_2[nLags];
    real sigma[nLags];
    real log_loss_paid[N];
    real mu[N];

    alpha[1] = 0.0;
    alpha[2:nYears] = alpha_free;

    beta[1:(nLags-1)] = beta_free;
    beta[nLags] = 0.0;

    sigma_2[nLags] = gamma_cdf(1 / sigma_raw[nLags], 1, 1);
    for (offset in 1:(nLags - 1)) {
        int i = nLags - offset;
        sigma_2[i] = sigma_2[i + 1] + gamma_cdf(1 / sigma_raw[i], 1, 1);
    }

    for (i in 1:nLags) {
        sigma[i] = sqrt(sigma_2[i]);
    }

    log_loss_paid[known_loss_ndxs] = log_loss_paid_known;
    log_loss_paid[pred_loss_ndxs] = log_loss_paid_pred;

    for (n in 1:N) {
        mu[n] = log_premium[n] + log_ELR + alpha[year[n]]
                + beta[dev_lag[n]] * (1 - gamma) ^ (year[n] - 1);
    }
}

model {
    alpha_free ~ normal(0, 3.162);
    beta_free ~ normal(0, 3.162);
    gamma ~ normal(0, 0.05);
    log_ELR ~ normal(-0.4, 3.162);

    sigma_raw ~ inv_gamma(1, 1);

    log_loss_paid ~ normal(mu, sigma[dev_lag]);
}
