data {
    int <lower=1> nObs;
    int <lower=1> N;

    real log_loss_paid_known[nObs];
    real log_loss_inc_known[nObs];
    int<lower=1, upper=N> known_loss_ndxs[nObs];

    real log_premium[N];
    int<lower=0, upper=1> is_first_year[N];
    int<lower=1> year[N];
    int<lower=1> dev_lag[N];
}

transformed data {
    int nPred = N - nObs;
    int nYears = max(year);
    int nLags = max(dev_lag);
    int<lower=1, upper=N> pred_loss_ndxs[nPred];

    int i_ = 1;
    int iK = 1;
    int iP = 1;
    while (i_ <= N) {
        if (iK <= nObs && known_loss_ndxs[iK] == i_) {
            iK += 1;
        } else {
            pred_loss_ndxs[iP] = i_;
            iP += 1;
        }
        i_ += 1;
    }
}

parameters {
    real log_ELR;
    real log_ELR_delta;

    real alpha_free[nYears - 1];
    real beta_free_inc[nLags - 1];
    real beta_free_paid[nLags - 1];
    real<lower=0, upper=100000> sigma_raw_inc[nLags];
    real<lower=0, upper=100000> sigma_raw_paid[nLags];

    real gamma;
    real<lower=0, upper=1> rho_raw;

    real log_loss_inc_pred[nPred];
    real log_loss_paid_pred[nPred];
}

transformed parameters {
    real alpha[nYears];
    real beta_inc[nLags];
    real beta_paid[nLags];
    real sigma_2_inc[nLags];
    real sigma_inc[nLags];
    real sigma_2_paid[nLags];
    real sigma_paid[nLags];
    real log_loss_inc[N];
    real log_loss_paid[N];
    real mu_inc[N];
    real mu_paid[N];
    real <lower=-1,upper=1> rho;

    alpha[1] = 0.0;
    alpha[2:nYears] = alpha_free;

    beta_inc[1:(nLags-1)] = beta_free_inc;
    beta_inc[nLags] = 0.0;
    beta_paid[1:(nLags-1)] = beta_free_paid;
    beta_paid[nLags] = 0.0;

    sigma_2_inc[nLags] = gamma_cdf(1 / sigma_raw_inc[nLags], 1, 1);
    for (offset in 1:(nLags - 1)) {
        int i = nLags - offset;
        sigma_2_inc[i] = sigma_2_inc[i + 1] + gamma_cdf(1 / sigma_raw_inc[i], 1, 1);
    }

    sigma_2_paid[nLags] = gamma_cdf(1 / sigma_raw_paid[nLags], 1, 1);
    for (offset in 1:(nLags - 1)) {
        int i = nLags - offset;
        sigma_2_paid[i] = sigma_2_paid[i + 1] + gamma_cdf(1 / sigma_raw_paid[i], 1, 1);
    }

    for (i in 1:nLags) {
        sigma_inc[i] = sqrt(sigma_2_inc[i]);
        sigma_paid[i] = sqrt(sigma_2_paid[i]);
    }

    rho = 2 * rho_raw - 1;

    log_loss_paid[known_loss_ndxs] = log_loss_paid_known;
    log_loss_paid[pred_loss_ndxs] = log_loss_paid_pred;
    log_loss_inc[known_loss_ndxs] = log_loss_inc_known;
    log_loss_inc[pred_loss_ndxs] = log_loss_inc_pred;

    mu_inc[1] = log_premium[1] + log_ELR + beta_inc[dev_lag[1]];
    for (n in 2:N) {
        mu_inc[n] = log_premium[n] + log_ELR + alpha[year[n]] + beta_inc[dev_lag[n]]
                + is_first_year[n] * rho * (log_loss_inc[n-1] - mu_inc[n-1]);
    }
    for (n in 1:N) {
        mu_paid[n] = log_premium[n] + log_ELR + log_ELR_delta + alpha[year[n]]
                + beta_paid[dev_lag[n]] * (1 - gamma) ^ (year[n] - 1);
    }
}

model {
    alpha_free ~ normal(0, 3.162);
    beta_free_inc ~ normal(0, 3.162);
    beta_free_paid ~ normal(0, 3.162);
    gamma ~ normal(0, 0.05);
    rho_raw ~ beta(2, 2);
    log_ELR ~ normal(-0.4, 3.162);
    log_ELR_delta ~ normal(0, 0.05);

    sigma_raw_inc ~ inv_gamma(1, 1);
    sigma_raw_paid ~ inv_gamma(1, 1);

    log_loss_inc ~ normal(mu_inc, sigma_inc[dev_lag]);
    log_loss_paid ~ normal(mu_paid, sigma_paid[dev_lag]);
}
