data {
  int<lower=0> N;         //# rows in triangle
  int<lower=0> U;         //# columns in triangle
  matrix[N,U] y;          //observations - triangle imbedded in rectngle
  real<lower=0> shrink_p;
  real<lower=0> shrink_q;    //shrinkage sd - currently data read in
  real<lower=0> shrink_r;
}
transformed data {

  int z[100,2];  //index for rows and columns of each actual observation
  int k;        //# observations in y
  k = 0;
  for (i in 1:10) {
    for (j in 1:10) {
      if (y[i,j] < 998) {
       k = k + 1;
       z[k,1] = i;
       z[k,2] = j;
      }
    }
  }
}
parameters {
  vector[9] b;                   //CY = diag params - 9 slope changes
  vector[9] c;                   //AY = cohort params - 9 slope changes
  vector[10] a;                   //DY = age params - slope changes
  real<lower=-30, upper=30> log_cn;                        //constant term
  real<lower=-5, upper=5> log_s;                         //residual variance scale
  real<lower=-5, upper=5> o;                        //residual variance power
  real<lower=-10, upper=10> log_sigma_y;     //log stdev of non-positive data residuals
}
transformed parameters {
  vector[10] p;                   //AY = cohort levels
  vector[10] r;                    //CY levels
  vector[10] q;                    //DY levels = age mortality base
  real w1;                         //trends
  real p1;
  real s;
  real cn;
  real sigma_y;
  matrix[N,U] m;         // fitted means
  sigma_y = exp(log_sigma_y);     //makes prior unbiased  //added for loo
  s = exp(log_s);
  cn = exp(log_cn);
  w1 = 0;
  p[1] = 1;
  // if (y[1,3] == 0)
  //   p[1] = 0;
  q[1] = a[1];
  r[1] = 1;

  //cohorts = acc years
  for (n in 2:10) {
    w1 = w1 + c[n-1];
    p[n] = p[n-1] + w1;  //Accumulates slope changes to levels
  }
  w1 = 0;
  p1 = 0;
  for (n in 1:10) {
    p1 = p1 + p[n] * (2*n - 11);
    w1 = w1 + p[n];
  }
  p1 = p1/165;
  w1 = w1/10 - p1*5.5;
  for (n in 1:10) {
    p[n] = p[n] - p1*n - w1; // residuals of linear
  }
  w1 = min(p);
  for (n in 1:10) {
    p[n] = p[n] - w1 + 1;
  }

  //ages
  w1 = 0;
  for (n in 2:10) {
    w1 = w1 + a[n-1];
    q[n] = q[n-1] + w1;
  }
  w1 = min(q);
  for (n in 1:10) {
    q[n] = q[n] - w1 + 0.1;     // age weights add to one
  }
  w1 = sum(q);
  for (n in 1:10) {
    q[n] = q[n]/w1;
  }

  //trend
  w1 = 0;
  for (n in 2:10) {
    w1 = w1 + b[n-1];
    r[n] = r[n-1] + w1;
  }
  w1 = min(r);
  for (n in 1:10) {
    r[n] = r[n] - w1 + 1;
  }
  w1 = r[1];
  for (n in 1:10) {
    r[n] = r[n]/w1;
  }

  //means
  for (n in 1:10) {
    for (u in 1:10) {
      if (n+u > 11) m[n,u] = cn*p[n]*q[u]*r[10];  //Testing part of the triangle
      if (n+u <= 11) m[n,u] = cn*p[n]*q[u]*r[n+u -1]; //Training part of the triangle

    }
  }
}
// Priors not specified are uniform over their defined ranges, like non-negative reals for sigma_y; transformed parameters are simulated but don't have priors.
// The first a, b and beta are thus uniform and not shrunk towards zero
model {
  for (i in 1:9)  b[i] ~ double_exponential(0, shrink_r);
  for (i in 1:9)  c[i] ~ double_exponential(0, shrink_p);  //LASSO-like
  for (i in 1:10)  a[i] ~ double_exponential(0, shrink_q);
  for (n in 1:10) {
    for (u in 1:11-n) {
      if (y[n,u]<0.000001)
      y[n,u] ~ normal(m[n,u], sigma_y);
      if (y[n,u] < 998 && y[n,u]>0.000001)    // makes lognormal mean m and variance sm^o
        y[n,u] ~ lognormal(log(m[n,u])-0.5*log(1+s*m[n,u]^(o-2)),(log(1+s*m[n,u]^(o-2)))^0.5);
    }
  }
} //Only data in triangle is modeled; additive model in logs
generated quantities {
  matrix[N,U] LR;
  vector[N] ULR;
  vector[k] log_lik;

  // Sample Full matrix

  for (n in 1:10){
    for (u in 1:10) {
      LR[n,u] = 777;
    }
  }
  for (n in 1:10) {
    for (u in 1:11-n) {
      LR[n,u] = y[n,u];
    }
    if (n>1){
      for (u in (11-n+1):10){
      LR[n,u] = lognormal_rng(log(m[n,u])-0.5*log(1+s*m[n,u]^(o-2)),(log(1+s*m[n,u]^(o-2)))^0.5);
    }
    }
  }


  // ULR sample for each accident_year
  for (i in 1:N){
    ULR[i]=0;
    for (j in 1:U){
      ULR[i] = ULR[i] + LR[i,j];
    }
  }

  // log likelihoods

  for (n in 1:k) {
    log_lik[n] = 0;
    if (y[z[n,1],z[n,2]]<0.000001)
      log_lik[n] = normal_lpdf(y[z[n,1],z[n,2]] | m[z[n,1],z[n,2]], sigma_y);
    if (y[z[n,1],z[n,2]] < 998 && y[z[n,1],z[n,2]]>0 && m[z[n,1],z[n,2]]>0)    // makes lognormal mean m and variance sm^r
       log_lik[n] = lognormal_lpdf(y[z[n,1],z[n,2]] | log(m[z[n,1],z[n,2]])-0.5*log(1+s*m[z[n,1],z[n,2]]^(o-2)), log(1+s*m[z[n,1],z[n,2]]^(o-2))^0.5);
  }
}

