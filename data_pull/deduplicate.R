deduplicate <- function(df, threshold=0.999) {
    df <- df %>%
        group_by(SNL_key, LOB, data_year) %>%
        mutate(total_EP = sum(EP)) %>%
        ungroup() %>%
        mutate(paid_LR = paid_LR / 100) %>%
        dcast(SNL_key + LOB + data_year + total_EP
              ~ dev_lag + data_lag, value.var="paid_LR") %>%
        mutate(ndx = 1:n())

    sim.mat <- build_similarity_matrix(df)
    cliques <- find_cliques(sim.mat, threshold)
    deduped.df <- merge_cliques(df, cliques)
    deduped.df %>%
        select(SNL_key, LOB, data_year)
}

build_similarity_matrix <- function(df) {
    df.mat <- as.matrix(df[, 5:59])
    sim.mat <- matrix(nrow=nrow(df), ncol=nrow(df))
    for (i in 2:nrow(df)) {
        for (j in 1:(i-1)) {
            sim <- cos_sim(df.mat[i, ], df.mat[j, ])
            sim.mat[i, j] <- sim
            sim.mat[j, i] <- sim
        }
    }
    sim.mat
}

find_cliques <- function(sim.mat, threshold) {
    (sim.mat > threshold) %>%
        dplyr::as_data_frame() %>%
        mutate(from = row_number()) %>%
        gather(key="to", value="edge", -from) %>%
        filter(edge) %>%
        mutate(to = as.integer(substring(to, 2, nchar(to)))) %>%
        graph_from_data_frame(directed=FALSE) %>%
        max_cliques() %>%
        lapply(function(clique) as.numeric(names(clique))) %>%
        `[`(order(sapply(., length), decreasing=TRUE))
}

merge_cliques <- function(df, cliques) {
    pool <- 1:nrow(df)
    cleans <- c()

    for (clique in cliques) {
        filtered.clique <- intersect(clique, pool)
        pool <- setdiff(pool, clique)

        if (length(filtered.clique) > 0) {
            biggest.member <- df[filtered.clique, ] %>%
                arrange(desc(total_EP)) %>%
                slice(1) %>%
                extract2("ndx")
            cleans <- c(cleans, biggest.member)
        }
    }
    cleans <- c(cleans, pool)

    df[cleans, ]
}

cos_sim <- function(x, y) sum(x * y) / sqrt(sum(x*x) * sum(y*y))
