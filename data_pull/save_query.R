save_query <- function(query, output.file, creds=parseTOML("creds.toml")) {
    drv <- dbDriver("PostgreSQL")
    dbc <- dbConnect(drv, host="40.79.61.161", port=5432, dbname="snl_data",
                     user=creds$user, password=creds$password)

    time.info <- system.time(query.result <- dbGetQuery(dbc, query))
    message(sprintf("Read %d rows and %d columns in %0.02f seconds.",
                    nrow(query.result), ncol(query.result), time.info["elapsed"]))
    write_csv(query.result, output.file)
}
