functions {
    matrix super_correl_matrix(vector rho, matrix corr) {
        int K = num_elements(rho) + 1;
        int N = rows(corr);
        matrix[K*N, K*N] result;
        real lag_corr;

        // Equal lags
        for (iK in 1:K) {
            for (iN in 1:N) {
                for (jN in 1:N) {
                    result[(iN - 1) * K + iK, (jN - 1) * K + iK] = corr[iN, jN];
                }
            }
        }

        // Unequal lags
        for (iK in 1:(K-1)) {
            for (jK in (iK+1):K) {
                lag_corr = 1;
                for (k in (iK):(jK-1)) {
                    lag_corr *= rho[k];
                }
                for (iN in 1:N) {
                    for (jN in 1:N) {
                        // Upper triangle
                        result[(iN - 1) * K + iK, (jN - 1) * K + jK] = corr[iN, jN] * lag_corr;
                        // Lower triangle, note flipped iK/jK indices!
                        result[(iN - 1) * K + jK, (jN - 1) * K + iK] = corr[iN, jN] * lag_corr;
                    }
                }
            }
        }

        return result;
    }
}

data {
    // Constraints
    int<lower=1> N;                 // Number of observations
    int<lower=1> nIncurredUpdates;  // Number of observations with meaningful LR updates
    int<lower=1> nCompanies;        // Number of companies
    int<lower=1> nLags;             // Number of development lags
    int<lower=2> nIncurredPreds;    // Number of incurred predictors
    int<lower=2> nCompanyPreds;     // Number of company-level predictors

    // Response
    int<lower=0, upper=1> LR_incurred_update[N];    // Boolean indicator for meaningful LR update
    real LR_incurred_actual[nIncurredUpdates];      // Incurred LR response

    // Inputs
    matrix[N, nIncurredPreds] LR_incurred_input;            // Predictor matrix for incurred LR
    int<lower=1, upper=nCompanies> company_id[N];           // Company for each observation
    int<lower=1, upper=nLags> lag_id[N];                    // Lag for each observation
    matrix[nCompanies, nCompanyPreds] company_input;        // Company-level input matrix
}

transformed data {
    int<lower=1, upper=N> updateNdx[nIncurredUpdates];  // Array of indices where
                                                        // LR_incurred_update=1
    int nParams = nIncurredPreds + 2;                   // Extra params for sigma and update logit
    int nTotalParams = nParams * nLags;

    // Populate updateNdx array
    int ndx = 1;
    for (n in 1:N) {
        if (LR_incurred_update[n] == 1) {
            updateNdx[ndx] = n;
            ndx += 1;
        }
    }
}

parameters {
    // Main model parameters
    matrix[nCompanyPreds, nTotalParams] company_beta;   // Company predictors -> obs parameter means
    matrix[nTotalParams, nCompanies] obs_beta_norm;     // Obs parameter residuals
    cholesky_factor_corr[nParams] beta_corr_chol;       // Obs parameter residual correlations
    vector<lower=-1, upper=1>[nLags-1] beta_lag_rho;    // Obs param resid corr as function of lag
    vector<lower=0>[nTotalParams] beta_prior_scale;     // Prior scale for obs parameter resids
}

transformed parameters {
    // Primary effect parameters
    corr_matrix[nParams] beta_corr;
    matrix[nTotalParams, nTotalParams] beta_covar;
    matrix[nTotalParams, nTotalParams] beta_covar_chol;
    matrix[nCompanies, nTotalParams] obs_beta_flat;
    vector[nCompanies] obs_sigma[nLags];
    vector[nCompanies] obs_update_logit[nLags];
    matrix[nCompanies, nIncurredPreds] obs_beta[nLags];

    beta_corr = multiply_lower_tri_self_transpose(beta_corr_chol);
    beta_covar = quad_form_diag(super_correl_matrix(beta_lag_rho, beta_corr), beta_prior_scale);
    beta_covar_chol = cholesky_decompose(beta_covar);
    obs_beta_flat = company_input * company_beta + (beta_covar_chol * obs_beta_norm)';

    // Extract parameters from obs_beta_flat
    for (k in 1:nLags) {
        obs_sigma[k] = exp(obs_beta_flat[, k]);
        obs_update_logit[k] = obs_beta_flat[, nLags + k];
        for (n in 1:nIncurredPreds) {
            obs_beta[k, , n] = obs_beta_flat[, (n+1) * nLags + k];
        }
    }
}

model {
    // Predictions
    real LR_incurred_update_logit[N];   // Logit of incurred update
    real LR_incurred_pred[N];           // Predicted incurred LR
    real LR_incurred_sigma[N];          // Predicted SD of incurred LR

    // Compute predictions
    for (n in 1:N) {
        LR_incurred_update_logit[n] = obs_update_logit[lag_id[n], company_id[n]];
        LR_incurred_pred[n] = LR_incurred_input[n, ] * obs_beta[lag_id[n], company_id[n]]';
        LR_incurred_sigma[n] = obs_sigma[lag_id[n], company_id[n]];
    }

    // Distributions on parameters ////////////////////////////////////////////////////////////////

    // Priors for mean of sigma
    for (k in 1:nLags) {
        company_beta[1, k] ~ normal(-2, 3);
    }
    // Priors for mean of logit
    for (k in (nLags+1):(2*nLags)) {
        company_beta[1, k] ~ normal(0, 5);
    }
    // Priors for mean of all other params
    for (k in (2*nLags+1):(nLags*nParams)) {
        company_beta[1, k] ~ normal(0, 1);
    }
    // Priors for company-level predictor effects
    for (n in 1:nCompanyPreds) {
        company_beta[n, ] ~ normal(0, 1);
    }

    // Priors on company-level parameters
    to_vector(obs_beta_norm) ~ normal(0, 1);
    beta_corr_chol ~ lkj_corr_cholesky(5);
    beta_prior_scale ~ cauchy(0, 1);

    // Key sampling statements ////////////////////////////////////////////////////////////////////

    LR_incurred_update ~ bernoulli_logit(LR_incurred_update_logit);
    LR_incurred_actual ~ normal(LR_incurred_pred[updateNdx], LR_incurred_sigma[updateNdx]);
}
