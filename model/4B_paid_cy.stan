functions {
    matrix super_correl_matrix(vector rho, matrix corr) {
        int K = num_elements(rho) + 1;
        int N = rows(corr);
        matrix[K*N, K*N] result;
        real lag_corr;

        // Equal lags
        for (iK in 1:K) {
            for (iN in 1:N) {
                for (jN in 1:N) {
                    result[(iN - 1) * K + iK, (jN - 1) * K + iK] = corr[iN, jN];
                }
            }
        }

        // Unequal lags
        for (iK in 1:(K-1)) {
            for (jK in (iK+1):K) {
                lag_corr = 1;
                for (k in (iK):(jK-1)) {
                    lag_corr *= rho[k];
                }
                for (iN in 1:N) {
                    for (jN in 1:N) {
                        // Upper triangle
                        result[(iN - 1) * K + iK, (jN - 1) * K + jK] = corr[iN, jN] * lag_corr;
                        // Lower triangle, note flipped iK/jK indices!
                        result[(iN - 1) * K + jK, (jN - 1) * K + iK] = corr[iN, jN] * lag_corr;
                    }
                }
            }
        }

        return result;
    }
}

data {
    // Constraints
    int<lower=1> N;                     // Number of observations
    int<lower=1> nPaidUpdates;          // Number of observations with meaningful LR updates
    int<lower=1> nCompanies;            // Number of companies
    int<lower=2> nLags;                 // Number of development lags
    int<lower=2, upper=nLags-2> nDeepLags;
    int<lower=1> nTailPaidPreds;        // Number of incurred predictors
    int<lower=nTailPaidPreds> nDeepPaidPreds;
    int<lower=2> nCompanyPreds;         // Number of company-level predictors
    int<lower=2> nCalendarYears;    // Number of calendar years

    // Response
    int<lower=0, upper=1> LR_paid_update[N];    // Boolean indicator for meaningful LR update
    real LR_paid_actual[nPaidUpdates];      // Incurred LR response

    // Inputs
    matrix[N, nDeepPaidPreds] LR_paid_input;            // Predictor matrix for incurred LR
    int<lower=1, upper=nCompanies> company_id[N];           // Company for each observation
    int<lower=1, upper=nLags> lag_id[N];                    // Lag for each observation
    int<lower=1, upper=nCalendarYears> calendar_year_id[N]; // Calendar year for each observation
    matrix[nCompanies, nCompanyPreds] company_input;        // Company-level input matrix
}

transformed data {
    int<lower=1, upper=N> updateNdx[nPaidUpdates];  // Array of indices where
                                                        // LR_incurred_update=1

    int nTailLags = nLags - nDeepLags;
    int nScaleParams = nLags * 2;
    int nDeepParams = nDeepLags * nDeepPaidPreds;
    int nTailParams = nTailLags * nTailPaidPreds;
    int nTotalParams = nScaleParams + nDeepParams + nTailParams;

    // Populate updateNdx array
    int ndx = 1;
    for (n in 1:N) {
        if (LR_paid_update[n] == 1) {
            updateNdx[ndx] = n;
            ndx += 1;
        }
    }
}

parameters {
    // Main model parameters
    matrix[nCompanyPreds, nTotalParams] company_beta;   // Company predictors -> obs parameter means
    matrix[nScaleParams, nCompanies] obs_scale_beta_norm;
    matrix[nDeepParams, nCompanies] obs_deep_beta_norm;
    matrix[nTailParams, nCompanies] obs_tail_beta_norm;

    cholesky_factor_corr[2] scale_corr_chol;
    cholesky_factor_corr[nDeepPaidPreds] deep_corr_chol;
    cholesky_factor_corr[nTailPaidPreds] tail_corr_chol;

    vector<lower=-1, upper=1>[nLags-1] scale_rho;
    vector<lower=-1, upper=1>[nDeepLags-1] deep_rho;
    vector<lower=-1, upper=1>[nTailLags-1] tail_rho;

    vector<lower=0>[nScaleParams] scale_scale;
    vector<lower=0>[nDeepParams] deep_scale;
    vector<lower=0>[nTailParams] tail_scale;

    // Calendar year effect parameters
    matrix[nCalendarYears-1, nCompanies] alpha_norm;      // Normed calendar year parameters
    vector[nCalendarYears-1] alpha_mu;
    vector<lower=0>[nCalendarYears-1] alpha_sigma;
    vector<lower=0>[nLags] alpha_lag_scale;
}

transformed parameters {
    corr_matrix[2] scale_corr;
    corr_matrix[nDeepPaidPreds] deep_corr;
    corr_matrix[nTailPaidPreds] tail_corr;

    matrix[nScaleParams, nScaleParams] scale_covar;
    matrix[nScaleParams, nScaleParams] scale_covar_chol;
    matrix[nDeepParams, nDeepParams] deep_covar;
    matrix[nDeepParams, nDeepParams] deep_covar_chol;
    matrix[nTailParams, nTailParams] tail_covar;
    matrix[nTailParams, nTailParams] tail_covar_chol;

    matrix[nCompanies, nTotalParams] beta_mu;
    matrix[nCompanies, nScaleParams] scale_params_flat;
    matrix[nCompanies, nDeepParams] deep_params_flat;
    matrix[nCompanies, nTailParams] tail_params_flat;

    vector[nCompanies] obs_sigma[nLags];
    vector[nCompanies] obs_update_logit[nLags];
    matrix[nCompanies, nDeepPaidPreds] obs_deep_beta[nDeepLags];
    matrix[nCompanies, nTailPaidPreds] obs_tail_beta[nTailLags];

    // Calendar year effect parameters
    matrix[nCalendarYears, nCompanies] alpha;

    scale_corr = multiply_lower_tri_self_transpose(scale_corr_chol);
    deep_corr = multiply_lower_tri_self_transpose(deep_corr_chol);
    tail_corr = multiply_lower_tri_self_transpose(tail_corr_chol);

    scale_covar = quad_form_diag(super_correl_matrix(scale_rho, scale_corr), scale_scale);
    deep_covar = quad_form_diag(super_correl_matrix(deep_rho, deep_corr), deep_scale);
    tail_covar = quad_form_diag(super_correl_matrix(tail_rho, tail_corr), tail_scale);

    scale_covar_chol = cholesky_decompose(scale_covar);
    deep_covar_chol = cholesky_decompose(deep_covar);
    tail_covar_chol = cholesky_decompose(tail_covar);

    beta_mu = company_input * company_beta;
    scale_params_flat = beta_mu[, 1:nScaleParams] + (scale_covar_chol * obs_scale_beta_norm)';
    deep_params_flat = beta_mu[, (nScaleParams+1):(nScaleParams + nDeepParams)]
            + (deep_covar_chol * obs_deep_beta_norm)';
    tail_params_flat = beta_mu[, (nScaleParams + nDeepParams + 1):nTotalParams]
            + (tail_covar_chol * obs_tail_beta_norm)';

    // Extract parameters
    for (k in 1:nLags) {
        obs_sigma[k] = exp(scale_params_flat[, k]);
        obs_update_logit[k] = scale_params_flat[, nLags + k];
    }

    for (k in 1:nDeepLags) {
        for (n in 1:nDeepPaidPreds) {
            obs_deep_beta[k, , n] = deep_params_flat[, (n-1) * nDeepLags + k];
        }
    }

    for (k in 1:nTailLags) {
        for (n in 1:nTailPaidPreds) {
            obs_tail_beta[k, , n] = tail_params_flat[, (n-1) * nTailLags + k];
        }
    }

    // Construct calendar year parameters
    for (n in 1:nCompanies) {
        alpha[1, n] = 0;
    }
    for (n in 1:(nCalendarYears-1)) {
        alpha[n+1, ] = alpha_mu[n] + alpha_norm[n, ] * alpha_sigma[n];
    }
}

model {
    // Predictions
    real LR_paid_update_logit[N];   // Logit of incurred update
    real LR_paid_pred[N];           // Predicted incurred LR
    real LR_paid_sigma[N];          // Predicted SD of incurred LR

    // Compute predictions
    for (n in 1:N) {
        LR_paid_update_logit[n] = obs_update_logit[lag_id[n], company_id[n]];
        LR_paid_sigma[n] = obs_sigma[lag_id[n], company_id[n]];
        if (lag_id[n] <= nDeepLags) {
            LR_paid_pred[n] = LR_paid_input[n, ] * obs_deep_beta[lag_id[n], company_id[n]]'
                    + alpha[calendar_year_id[n], company_id[n]] * alpha_lag_scale[lag_id[n]];
        } else {
            LR_paid_pred[n] = LR_paid_input[n, 1:nTailPaidPreds]
                    * obs_tail_beta[lag_id[n] - nDeepLags, company_id[n]]'
                    + alpha[calendar_year_id[n], company_id[n]] * alpha_lag_scale[lag_id[n]];
        }
    }

    // Distributions on parameters ////////////////////////////////////////////////////////////////

    // Priors for mean of sigma
    for (k in 1:nLags) {
        company_beta[1, k] ~ normal(-2, 3);
    }
    // Priors for mean of logit
    for (k in (nLags+1):(2*nLags)) {
        company_beta[1, k] ~ normal(0, 5);
    }
    // Priors for mean of all other params
    for (k in (2*nLags+1):(nTotalParams)) {
        company_beta[1, k] ~ normal(0, 1);
    }
    // Priors for company-level predictor effects
    for (n in 2:nCompanyPreds) {
        company_beta[n, ] ~ normal(0, 1);
    }

    // Priors on company-level parameters
    to_vector(obs_scale_beta_norm) ~ normal(0, 1);
    to_vector(obs_deep_beta_norm) ~ normal(0, 1);
    to_vector(obs_tail_beta_norm) ~ normal(0, 1);
    scale_corr_chol ~ lkj_corr_cholesky(2);
    deep_corr_chol ~ lkj_corr_cholesky(2);
    tail_corr_chol ~ lkj_corr_cholesky(2);

    scale_scale ~ cauchy(0, 1);
    deep_scale ~ cauchy(0, 1);
    tail_scale ~ cauchy(0, 1);

    // Priors on calendar-year parameters
    to_vector(alpha_norm) ~ normal(0, 1);
    alpha_mu ~ normal(0, 0.1);
    alpha_sigma ~ cauchy(0, 0.3);
    alpha_lag_scale ~ cauchy(0, 1);

    // Key sampling statements ////////////////////////////////////////////////////////////////////

    LR_paid_update ~ bernoulli_logit(LR_paid_update_logit);
    LR_paid_actual ~ normal(LR_paid_pred[updateNdx], LR_paid_sigma[updateNdx]);
}
