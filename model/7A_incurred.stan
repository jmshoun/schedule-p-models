data {
    // Constraints
    int<lower=1> N;                     // Number of observations
    int<lower=1> nIncurredUpdates;      // Number of observations with meaningful LR updates
    int<lower=1> nCompanies;            // Number of companies
    int<lower=2> nLags;                 // Number of development lags
    int<lower=2, upper=nLags-2> nDeepLags;
    int<lower=1> nTailIncurredPreds;    // Number of incurred predictors
    int<lower=nTailIncurredPreds> nDeepIncurredPreds;
    int<lower=2> nCompanyPreds;         // Number of company-level predictors

    // Response
    int<lower=0, upper=1> LR_incurred_update[N];    // Boolean indicator for meaningful LR update
    real LR_incurred_actual[nIncurredUpdates];      // Incurred LR response

    // Inputs
    matrix[N, nDeepIncurredPreds] LR_incurred_input;        // Predictor matrix for incurred LR
    int<lower=1, upper=nCompanies> company_id[N];           // Company for each observation
    int<lower=1, upper=nLags> lag_id[N];                    // Lag for each observation
    matrix[nCompanies, nCompanyPreds] company_input;        // Company-level input matrix
}

transformed data {
    int<lower=1, upper=N> updateNdx[nIncurredUpdates];  // Array of indices where
                                                        // LR_incurred_update=1

    int nTailLags = nLags - nDeepLags;
    int nScaleParams = 2;
    int nDeepParams = nDeepLags * nDeepIncurredPreds;
    int nTailParams = nTailLags * nTailIncurredPreds;
    int nTotalParams = nScaleParams + nDeepParams + nTailParams;

    // Populate updateNdx array
    int ndx = 1;
    for (n in 1:N) {
        if (LR_incurred_update[n] == 1) {
            updateNdx[ndx] = n;
            ndx += 1;
        }
    }
}

parameters {
    // Main model parameters
    matrix[nCompanyPreds, nScaleParams + nDeepIncurredPreds + nTailIncurredPreds] company_beta;
    matrix[nScaleParams, nCompanies] company_scale_norm;
    matrix[nDeepIncurredPreds, nCompanies] company_deep_mu_norm;
    matrix[nTailIncurredPreds, nCompanies] company_tail_mu_norm;
    matrix[nDeepParams, nCompanies] deep_resid_norm;
    matrix[nTailParams, nCompanies] tail_resid_norm;

    cholesky_factor_corr[nScaleParams] scale_corr_chol;
    cholesky_factor_corr[nDeepIncurredPreds] deep_corr_mu_chol;
    cholesky_factor_corr[nTailIncurredPreds] tail_corr_mu_chol;

    vector<lower=0>[nDeepIncurredPreds] deep_mu_scale;
    vector<lower=0>[nTailIncurredPreds] tail_mu_scale;
    vector<lower=0>[nDeepParams] deep_resid_scale;
    vector<lower=0>[nTailParams] tail_resid_scale;
    vector<lower=0>[nLags] sigma_scale;
    vector<lower=0>[nLags] logit_scale;
}

transformed parameters {
    matrix[nDeepIncurredPreds, nCompanies] company_deep_mu_adj;
    matrix[nTailIncurredPreds, nCompanies] company_tail_mu_adj;
    matrix[nScaleParams, nCompanies] company_scale_adj;
    matrix[nTotalParams, nCompanies] company_mu;

    vector[nCompanies] obs_sigma[nLags];
    vector[nCompanies] obs_update_logit[nLags];
    matrix[nCompanies, nDeepIncurredPreds] obs_deep_beta[nDeepLags];
    matrix[nCompanies, nTailIncurredPreds] obs_tail_beta[nTailLags];

    company_deep_mu_adj = deep_corr_mu_chol * company_deep_mu_norm;
    company_tail_mu_adj = tail_corr_mu_chol * company_tail_mu_norm;
    company_scale_adj = scale_corr_chol * company_scale_norm;

    for (k in 1:nLags) {
        obs_sigma[k] = (company_mu[1, ] + sigma_scale[k] * company_scale_adj[1, ])';
        obs_update_logit[k] = (company_mu[2, ] + logit_scale[k] * company_scale_adj[2, ])';
    }

    for (k in 1:nDeepIncurredPreds) {
        for (n in 1:nDeepLags) {
            obs_deep_beta[n, , k] = (company_mu[nScaleParams + k, ]
                    + company_deep_mu_adj[k, ] * deep_mu_scale[k]
                    + deep_resid_norm[k * (nDeepLags - 1) + n, ]
                        * deep_resid_scale[k * (nDeepLags - 1) + n])';
        }
    }

    for (k in 1:nTailIncurredPreds) {
        for (n in 1:nTailLags) {
            obs_tail_beta[n, , k] = (company_mu[nScaleParams + nDeepIncurredPreds + k, ]
                    + company_tail_mu_adj[k, ] * tail_mu_scale[k]
                    + tail_resid_norm[k * (nTailLags - 1) + n, ]
                        * tail_resid_scale[k * (nTailLags - 1) + n])';
        }
    }
}

model {
    // Predictions
    real LR_incurred_update_logit[N];   // Logit of incurred update
    real LR_incurred_pred[N];           // Predicted incurred LR
    real LR_incurred_sigma[N];          // Predicted SD of incurred LR

    // Compute predictions
    for (n in 1:N) {
        LR_incurred_update_logit[n] = obs_update_logit[lag_id[n], company_id[n]];
        LR_incurred_sigma[n] = obs_sigma[lag_id[n], company_id[n]];
        if (lag_id[n] <= nDeepLags) {
            LR_incurred_pred[n] = LR_incurred_input[n, ] * obs_deep_beta[lag_id[n], company_id[n]]';
        } else {
            LR_incurred_pred[n] = LR_incurred_input[n, 1:nTailIncurredPreds]
                    * obs_tail_beta[lag_id[n] - nDeepLags, company_id[n]]';
        }
    }

    // Distributions on parameters ////////////////////////////////////////////////////////////////

    // Priors for mean of sigma
    company_beta[1, 1] ~ normal(-2, 3);
    company_beta[1, 2] ~ normal(0, 5);
    // Priors for mean of all other params
    for (k in 3:(nScaleParams + nDeepParams + nTailParams)) {
        company_beta[1, k] ~ normal(0, 1);
    }
    // Priors for company-level predictor effects
    for (n in 2:nCompanyPreds) {
        company_beta[n, ] ~ normal(0, 1);
    }

    to_vector(company_scale_norm) ~ normal(0, 1);
    to_vector(company_deep_mu_norm) ~ normal(0, 1);
    to_vector(company_tail_mu_norm) ~ normal(0, 1);
    to_vector(deep_resid_norm) ~ normal(0, 1);
    to_vector(tail_resid_norm) ~ normal(0, 1);

    scale_corr_chol ~ lkj_corr_cholesky(2);
    deep_corr_mu_chol ~ lkj_corr_cholesky(2);
    tail_corr_mu_chol ~ lkj_corr_cholesky(2);

    deep_mu_scale ~ cauchy(0, 1);
    tail_mu_scale ~ cauchy(0, 1);
    deep_resid_scale ~ cauchy(0, 1);
    tail_resid_scale ~ cauchy(0, 1);
    sigma_scale ~ cauchy(0, 1);
    logit_scale ~ cauchy(0, 1);

    // Key sampling statements ////////////////////////////////////////////////////////////////////

    LR_incurred_update ~ bernoulli_logit(LR_incurred_update_logit);
    LR_incurred_actual ~ normal(LR_incurred_pred[updateNdx], LR_incurred_sigma[updateNdx]);
}
