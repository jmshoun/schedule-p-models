data {
    // Constraints
    int<lower=1> N;                     // Number of observations
    int<lower=1> nIncurredUpdates;      // Number of observations with meaningful LR updates
    int<lower=1> nCompanies;            // Number of companies
    int<lower=2> nLags;                 // Number of development lags
    int<lower=2, upper=nLags-2> nDeepLags;
    int<lower=1> nTailIncurredPreds;    // Number of incurred predictors
    int<lower=nTailIncurredPreds> nDeepIncurredPreds;
    int<lower=2> nCompanyPreds;         // Number of company-level predictors

    // Response
    int<lower=0, upper=1> LR_incurred_update[N];    // Boolean indicator for meaningful LR update
    real LR_incurred_actual[nIncurredUpdates];      // Incurred LR response

    // Inputs
    matrix[N, nDeepIncurredPreds] LR_incurred_input;        // Predictor matrix for incurred LR
    int<lower=1, upper=nCompanies> company_id[N];           // Company for each observation
    int<lower=1, upper=nLags> lag_id[N];                    // Lag for each observation
    matrix[nCompanies, nCompanyPreds] company_input;        // Company-level input matrix
}

transformed data {
    int<lower=1, upper=N> updateNdx[nIncurredUpdates];  // Array of indices where
                                                        // LR_incurred_update=1

    int nTailLags = nLags - nDeepLags;
    int nScaleParams = 2;
    int nDeepParams = nDeepLags * nDeepIncurredPreds;
    int nTailParams = nTailLags * nTailIncurredPreds;
    int nTotalParams = nScaleParams + nDeepParams + nTailParams;

    // Populate updateNdx array
    int ndx = 1;
    for (n in 1:N) {
        if (LR_incurred_update[n] == 1) {
            updateNdx[ndx] = n;
            ndx += 1;
        }
    }
}

parameters {
    // Main model parameters
    vector[nCompanies] company_scale_norm;
    vector[nCompanies] company_logit_norm;
    matrix[nCompanies, nDeepParams] company_deep_norm;
    matrix[nCompanies, nTailParams] company_tail_norm;

    vector[nLags] company_scale_mu;
    vector[nLags] company_logit_mu;
    vector[nDeepParams] company_deep_mu;
    vector[nTailParams] company_tail_mu;

    vector<lower=0>[nLags] company_scale_sigma;
    vector<lower=0>[nLags] company_logit_sigma;
    vector<lower=0>[nDeepParams] company_deep_sigma;
    vector<lower=0>[nTailParams] company_tail_sigma;
}

transformed parameters {
    vector[nCompanies] obs_sigma[nLags];
    vector[nCompanies] obs_update_logit[nLags];
    matrix[nCompanies, nDeepIncurredPreds] obs_deep_beta[nDeepLags];
    matrix[nCompanies, nTailIncurredPreds] obs_tail_beta[nTailLags];

    for (k in 1:nLags) {
        obs_sigma[k] = exp(company_scale_mu[k] + company_scale_sigma[k] * company_scale_norm);
        obs_update_logit[k] = company_logit_mu[k] + company_logit_sigma[k] * company_logit_norm;
    }

    for (k in 1:nDeepLags) {
        for (n in 1:nDeepIncurredPreds) {
            obs_deep_beta[k, , n] = company_deep_mu[(n-1) * nDeepLags + k]
                    + company_deep_sigma[(n-1) * nDeepLags + k]
                        * company_deep_norm[, (n-1) * nDeepLags + k];
        }
    }

    for (k in 1:nTailLags) {
        for (n in 1:nTailIncurredPreds) {
            obs_tail_beta[k, , n] = company_tail_mu[(n-1) * nTailLags + k]
                    + company_tail_sigma[(n-1) * nTailLags + k]
                        * company_tail_norm[, (n-1) * nTailLags + k];
        }
    }
}

model {
    // Predictions
    real LR_incurred_update_logit[N];   // Logit of incurred update
    real LR_incurred_pred[N];           // Predicted incurred LR
    real LR_incurred_sigma[N];          // Predicted SD of incurred LR

    // Compute predictions
    for (n in 1:N) {
        LR_incurred_update_logit[n] = obs_update_logit[lag_id[n], company_id[n]];
        LR_incurred_sigma[n] = obs_sigma[lag_id[n], company_id[n]];
        if (lag_id[n] <= nDeepLags) {
            LR_incurred_pred[n] = LR_incurred_input[n, ] * obs_deep_beta[lag_id[n], company_id[n]]';
        } else {
            LR_incurred_pred[n] = LR_incurred_input[n, 1:nTailIncurredPreds]
                    * obs_tail_beta[lag_id[n] - nDeepLags, company_id[n]]';
        }
    }

    // Distributions on parameters ////////////////////////////////////////////////////////////////

    company_scale_norm ~ normal(0, 1);
    company_logit_norm ~ normal(0, 1);
    to_vector(company_deep_norm) ~ normal(0, 1);
    to_vector(company_tail_norm) ~ normal(0, 1);

    company_scale_mu ~ normal(-2, 3);
    company_logit_mu ~ normal(0, 5);
    company_deep_mu ~ normal(0, 0.2);
    company_tail_mu ~ normal(0, 0.2);

    company_scale_sigma ~ cauchy(0, 1);
    company_logit_sigma ~ cauchy(0, 1);
    company_deep_sigma ~ cauchy(0, 1);
    company_tail_sigma ~ cauchy(0, 1);

    LR_incurred_update ~ bernoulli_logit(LR_incurred_update_logit);
    LR_incurred_actual ~ normal(LR_incurred_pred[updateNdx], LR_incurred_sigma[updateNdx]);
}
