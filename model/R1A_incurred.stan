data {
    // Constraints
    int<lower=1> N;                     // Number of observations
    int<lower=1> nIncurredUpdates;      // Number of observations with meaningful LR updates
    int<lower=1> nCompanies;            // Number of companies
    int<lower=2> nLags;                 // Number of development lags

    // Response
    int<lower=0, upper=1> LR_incurred_update[N];    // Boolean indicator for meaningful LR update
    real LR_incurred_actual[nIncurredUpdates];      // Incurred LR response

    int<lower=1, upper=nCompanies> company_id[N];           // Company for each observation
    int<lower=1, upper=nLags> lag_id[N];                    // Lag for each observation
}

transformed data {
    int<lower=1, upper=N> updateNdx[nIncurredUpdates];  // Array of indices where
                                                        // LR_incurred_update=1

    // Populate updateNdx array
    int ndx = 1;
    for (n in 1:N) {
        if (LR_incurred_update[n] == 1) {
            updateNdx[ndx] = n;
            ndx += 1;
        }
    }
}

parameters {
    // Main model parameters
    vector[nCompanies] intercept_norm[nLags];
    vector[nCompanies] scale_norm;
    vector[nCompanies] logit_norm;

    vector[nLags] intercept_mu;
    vector[nLags] scale_mu;
    vector[nLags] logit_mu;

    vector<lower=0>[nLags] intercept_sigma;
    vector<lower=0>[nLags] scale_sigma;
    vector<lower=0>[nLags] logit_sigma;
}

transformed parameters {
    vector[nCompanies] obs_sigma[nLags];
    vector[nCompanies] obs_logit[nLags];
    vector[nCompanies] obs_intercept[nLags];

    for (k in 1:nLags) {
        obs_sigma[k] = exp(scale_mu[k] + scale_sigma[k] * scale_norm);
        obs_logit[k] = logit_mu[k] + logit_sigma[k] * logit_norm;
        obs_intercept[k] = intercept_mu[k] + intercept_sigma[k] * intercept_norm[k];
    }
}

model {
    // Predictions
    real LR_incurred_update_logit[N];   // Logit of incurred update
    real LR_incurred_pred[N];           // Predicted incurred LR
    real LR_incurred_sigma[N];          // Predicted SD of incurred LR

    // Compute predictions
    for (n in 1:N) {
        LR_incurred_update_logit[n] = obs_logit[lag_id[n], company_id[n]];
        LR_incurred_sigma[n] = obs_sigma[lag_id[n], company_id[n]];
        LR_incurred_pred[n] = obs_intercept[lag_id[n], company_id[n]];
    }

    // Distributions on parameters ////////////////////////////////////////////////////////////////

    for (n in 1:nLags) {
        intercept_norm[n] ~ normal(0, 1);
    }
    scale_norm ~ normal(0, 1);
    logit_norm ~ normal(0, 1);

    intercept_mu ~ normal(0, 1);
    scale_mu ~ normal(-2, 3);
    logit_mu ~ normal(0, 5);

    intercept_sigma ~ cauchy(0, 1);
    scale_sigma ~ cauchy(0, 1);
    logit_sigma ~ cauchy(0, 1);

    LR_incurred_update ~ bernoulli_logit(LR_incurred_update_logit);
    LR_incurred_actual ~ normal(LR_incurred_pred[updateNdx], LR_incurred_sigma[updateNdx]);
}
